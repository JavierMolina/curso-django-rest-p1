from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import CustomUserViewSet

router = DefaultRouter()
router.register('user', CustomUserViewSet, base_name='user')
urlpatterns = [
    path('users/', include(router.urls)),
]
