from rest_framework import  viewsets
from rest_framework.permissions import AllowAny
from .models import CustomUser
from .serializers import CustomUserSerializer

class CustomUserViewSet(viewsets.ModelViewSet):
    # permission_classes = (AllowAny,)
    # http_method_names = ['get', 'post', 'put', 'patch']
    serializer_class = CustomUserSerializer
    queryset = CustomUser.objects.all()
