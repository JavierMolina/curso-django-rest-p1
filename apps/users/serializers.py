from rest_framework import serializers
from django.db import IntegrityError, transaction
from rest_framework.exceptions import APIException
from .models import CustomUser


class CustomUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = [
            'id',
            'email',
            'date_joined',
            'is_staff',
            'is_active',
        ]
